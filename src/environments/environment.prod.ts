export const environment = {
  production: true,
  webserviceUrl: 'http://localhost:3000',
  socketUrl: 'http://localhost:5000'
};
