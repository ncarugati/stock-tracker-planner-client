import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public links: string[] = ['tracker', 'planner']

  public activeLink: string = this.links[0];

  public setActiveLink(link: string): void {
    this.activeLink = link;
  }

  public isActiveLink(link: string): boolean {
    return this.activeLink == link;
  }

}
