import { Component, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'quote-entry',
  templateUrl: './quote-entry.component.html',
  styleUrls: ['./quote-entry.component.css']
})
export class QuoteEntryComponent {

  constructor() { }

  @Input() form: FormGroup;

  @Output() onDelete: EventEmitter<string> = new EventEmitter<string>();

  sendDeleteSignal(): void {
    this.onDelete.emit('signal');
  }

}
