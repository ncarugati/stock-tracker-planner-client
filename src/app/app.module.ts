import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { RoutingModule } from './routing.module';
import { AppComponent } from './app.component';

import { PlannerComponent } from './workflows/planner/planner.component';
import { TrackerComponent } from './workflows/tracker/tracker.component';

import { PortfolioService } from './models/portfolio/portfolio.service';
import { TickerService } from './models/ticker/ticker.service';
import { MaterialModule } from './material.module';

import { QuoteEntryComponent } from './components/quote-entry.component';

@NgModule({
  declarations: [
    AppComponent,
    PlannerComponent,
    TrackerComponent,
    QuoteEntryComponent
  ],
  imports: [
    BrowserModule,
    RoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MaterialModule,
    BrowserAnimationsModule
  ],
  providers: [
    PortfolioService,
    TickerService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
