import { Injectable } from '@angular/core';

import { ApiService } from './../api/api.service';

@Injectable()
export class TickerService {

  constructor(private apiService: ApiService<any>) { }

  public getTickerQuotes(symbols: string[]) {
    const symbolList = symbols.join(',');
    const endpoint = '/quotes?symbols=' + symbolList;
    return this.apiService.get(endpoint);
  }

  public searchSymbols(symbol: string) {
    const endpoint = '/quotes/search?input=' + symbol;
    return this.apiService.get(endpoint);
  }
  
}
