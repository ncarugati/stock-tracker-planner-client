export interface Security {
  amount: number,
  symbol: string,
  closingBalance?: number
}
