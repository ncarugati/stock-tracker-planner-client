import { Security } from './security';

export interface Portfolio {
  name: string,
  securities: Security[]
}
