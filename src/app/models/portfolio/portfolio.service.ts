import { Injectable } from '@angular/core';

import { ApiService } from './../api/api.service';

import { Portfolio } from './../../models/portfolio/portfolio';

@Injectable()
export class PortfolioService {

  constructor(private apiService: ApiService<Portfolio>) {}

  public createPortfolio(portfolio: Portfolio) {
    const endpoint = '/portfolio';
    return this.apiService.post(endpoint, portfolio);
  }

  public getPortfolio(id: number) {
    const endpoint = '/portfolio/' + id;
    return this.apiService.get(endpoint);
  }

  public editPortfolio(id: number, portfolio: Portfolio) {
    const endpoint = '/portfolio/' + id;
    return this.apiService.put(endpoint, portfolio);
  }

  public deletePortfolio(id: number) {
    const endpoint = '/portfolio/' + id;
    return this.apiService.delete(endpoint);
  }


}
