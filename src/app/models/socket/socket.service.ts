import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { io, Socket } from 'socket.io-client/build/index';

import { environment } from '../../../environments/environment';

@Injectable()
export class SocketService {

  constructor() { }

  private socket: Socket;

  private connect() {
    this.socket = io(environment.socketUrl);
  }

  public listen(): Observable<MessageEvent> {
    if (!this.socket) {
      this.connect();
    }

    return new Observable<MessageEvent>((observer) => {
      this.socket.on('update', (message: any) => {
        observer.next(message);
      });
    })
  }

  public sendMessage(message: string) {
    if (!this.socket) {
      return;
    }
    this.socket.emit('message', message);
  }

}
