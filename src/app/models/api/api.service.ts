import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { tap } from 'rxjs/operators';
import { catchError } from 'rxjs/operators';

import { environment } from '../../../environments/environment';

@Injectable()
export class ApiService<T> {

  constructor(private http: HttpClient) { }

  public get(endpoint: string, headers?: HttpHeaders) {
    const options = this.getHeaders(headers);
    return this.http.get<T>(environment.webserviceUrl + endpoint, options).pipe(
      tap(data => data),
      catchError(error => error)
    );
  }

  public post(endpoint: string, body?: any, headers?: HttpHeaders) {
    const options = this.getHeaders(headers);
    return this.http.post<T>(environment.webserviceUrl + endpoint, body, options).pipe(
      tap(data => data),
      catchError(error => error)
    );;
  }

  public put(endpoint: string, body?: any, headers?: HttpHeaders) {
    const options = this.getHeaders(headers);
    return this.http.put<T>(environment.webserviceUrl + endpoint, body, options).pipe(
      tap(data => data),
      catchError(error => error)
    );;
  }

  public delete(endpoint: string, headers?: HttpHeaders) {
    const options = this.getHeaders(headers);
    return this.http.delete<T>(environment.webserviceUrl + endpoint, options).pipe(
      tap(data => data),
      catchError(error => error)
    );
  }

  private getHeaders(headers: HttpHeaders): any {
      let headerContainer: any = null;
      if (!headers) {
        headerContainer = {
            headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'Accept': 'application/json'
          })
        };
      } else {
        headerContainer = {headers: headers};
      }

      return headerContainer;
  }

}
