import { Component } from '@angular/core';

import { FormArray, FormGroup, FormControl } from '@angular/forms';

import { ViewChild } from '@angular/core';

import { QuoteEntryComponent } from './../../components/quote-entry.component';

@Component({
  selector: 'app-planner',
  templateUrl: './planner.component.html',
  styleUrls: ['./planner.component.css']
})
export class PlannerComponent {

  public formContainer: FormGroup;

  public quoteForm: FormArray;

  constructor() {
    this.quoteForm = new FormArray([]);
    this.formContainer = new FormGroup({
      items: this.quoteForm
    });
  }

  public addSecurity(): void {
    const form: FormGroup = new FormGroup({
        symbol: new FormControl(),
        amount: new FormControl()
    });

    this.quoteForm.push(form);
  }

  public removeSecurity(index: number): void {
    this.quoteForm.removeAt(index);
  }

}
