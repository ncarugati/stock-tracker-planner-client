import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PlannerComponent } from './workflows/planner/planner.component';
import { TrackerComponent } from './workflows/tracker/tracker.component';

const routes: Routes = [
  { path: '',   redirectTo: 'tracker', pathMatch: 'full' },
  { path: 'planner', component: PlannerComponent },
  { path: 'tracker', component: TrackerComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class RoutingModule { }
